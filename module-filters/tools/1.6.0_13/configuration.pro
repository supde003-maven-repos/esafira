-injars tools-1.6.0_13.jar(
    !META-INF/**,
    !org/relaxng/datatype/**,
    !sun/applet/**,
    !sun/tools/jar/**,
    !sun/tools/hprof/**
    )

-outjars tools-1.6.0_13-alt.jar

-dontshrink
-dontoptimize
-dontobfuscate
-dontwarn **

