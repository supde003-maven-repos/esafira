-injars xom-1.2.10.jar(
    !META-INF/**,
    !org/jaxen/**,
    !org/w3c/**,
    !nu/xom/benchmarks/**,
    !nu/xom/samples/**,
    !nu/xom/tests/**,
    !nu/xom/samples/**
    )

-outjars xom-1.2.10-alt.jar

-dontshrink
-dontoptimize
-dontobfuscate
-dontwarn **

