-injars junit-4.10.jar(
    !META-INF/**,
    !org/hamcrest/**
    )

-outjars junit-4.10-alt.jar

-dontshrink
-dontoptimize
-dontobfuscate
-dontwarn **

