-injars woden-impl-dom-1.0M8.jar(
    !META-INF/**,
    !javax/xml/namespace/**
    )

-outjars woden-impl-dom-1.0M8-alt.jar

-dontshrink
-dontoptimize
-dontobfuscate
-dontwarn **

