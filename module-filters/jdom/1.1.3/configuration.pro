-libraryjars
    ../../../esafira/jre_libs/1.6.13/rt.jar

-injars jdom-1.1.3.jar(
    !META-INF/**,
    **
    )

-injars jOpenDocument-1.3.jar(
    !META-INF/**,
    org/jdom/PFilterIterator.class
    )

-outjars jdom-1.1.3-alt.jar

-dontshrink
-dontoptimize
-dontobfuscate
-dontwarn **

