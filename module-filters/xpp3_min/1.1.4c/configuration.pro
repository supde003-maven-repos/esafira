-injars xpp3_min-1.1.4c.jar(
    !META-INF/**,
    !org/xmlpull/v1/**
    )

-outjars xpp3_min-1.1.4c-alt.jar

-dontshrink
-dontoptimize
-dontobfuscate
-dontwarn **

