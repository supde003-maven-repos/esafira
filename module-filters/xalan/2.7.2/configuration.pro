-injars xalan-2.7.2.jar(
    !META-INF/**,
    !org/apache/bcel/**,
    !org/apache/regexp/**
    )

-outjars xalan-2.7.2-alt.jar

-dontshrink
-dontoptimize
-dontobfuscate
-dontwarn **

