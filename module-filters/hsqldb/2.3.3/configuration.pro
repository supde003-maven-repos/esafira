-libraryjars
    ../../../esafira/jre_libs/1.6.13/rt.jar

-injars hsqldb-2.3.3.jar(
    !META-INF/**,
    **
    )

-injars hsqldb-1.7.3.3.jar(
    !META-INF/**,
    org/hsqldb/util/Transfer*.class,
    )

-outjars hsqldb-2.3.3-alt.jar

-dontshrink
-dontoptimize
-dontobfuscate
-dontwarn **

