-injars transaction-api-1.1.jar(
    !META-INF/**,
    !javax/transaction/InvalidTransactionException.class,
    !javax/transaction/TransactionRequiredException.class,
    !javax/transaction/TransactionRolledbackException.class,
    !javax/transaction/xa/XAException.class,
    !javax/transaction/xa/XAResource.class,
    !javax/transaction/xa/Xid.class
    )

-outjars transaction-api-1.1-alt.jar

-dontshrink
-dontoptimize
-dontobfuscate
-dontwarn **

