-injars jOpenDocument-1.3.jar(
    !META-INF/**,
    !org/jaxen/**,
    !org/jdom/**,
    !org/mozilla/**,
    !org/apache/commons/**,
    !org/w3c/dom/**
    )

-outjars jOpenDocument-1.3-alt.jar

-dontshrink
-dontoptimize
-dontobfuscate
-dontwarn **

