-injars axis2-kernel-1.5.6.jar(
    !META-INF/**,
    !org/apache/axis2/transport/http/**
    )

-outjars axis2-kernel-1.5.6-alt.jar

-dontshrink
-dontoptimize
-dontobfuscate
-dontwarn **

