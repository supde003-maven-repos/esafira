-injars jaxen-1.1.6.jar(
    !META-INF/**,
    !org/w3c/dom/**
    )

-outjars jaxen-1.1.6-alt.jar

-dontshrink
-dontoptimize
-dontobfuscate
-dontwarn **

