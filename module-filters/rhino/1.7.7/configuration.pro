-libraryjars
    ../../../esafira/jre_libs/1.6.13/rt.jar

-injars rhino-1.7.7.jar(
    !META-INF/**,
    !LICENSE.txt,
    **
    )

#deprecated classes
-injars rhino-1.7R5.jar(
    !META-INF/**,
    !LICENSE.txt,
    org/mozilla/javascript/ClassDefinitionException.class,
    org/mozilla/javascript/NotAFunctionException.class,
    org/mozilla/javascript/PropertyException.class
    )

-outjars rhino-1.7.7-alt.jar

-dontshrink
-dontoptimize
-dontobfuscate
-dontwarn **

